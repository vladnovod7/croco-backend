const { getWord } = require('./randomWordAPI');

function State({
  activeUsers = [],
  messagesList = [],
  gameData = {
    status: 'looking for players',
    roomKing: '',
  },
  word = '',
} = {}, io) {
  this._activeUsers = activeUsers;
  this._messagesList = messagesList;
  this._gameData = gameData;
  this._word = word;

  const isReadyToStart = () => this._gameData.status === 'looking for players' && this._activeUsers.length > 1;
  const isGameCantExist = () => this._gameData.status === 'in game' && this._activeUsers.length < 2
  const isUserAlreadyInList = (data) => !!this._activeUsers.find(({ id }) => id === data.id);
  const isWordMatched = (data) => data.text === this._word;
  const isEnoughUsersToStart = () => this._activeUsers.length > 1;
  const isMessagesListFull = () => this._messagesList.length > 4;

  const pickNewRoomKing = (matchedMessage) => this._activeUsers.find(({ email }) => matchedMessage.email === email).id;

  this.getActiveUsers = () => {
    io.emit("userlist update", this._activeUsers)
  };

  this.setActiveUsers = (users) => {
    this._activeUsers = users;

    io.emit("userlist update", users);
  }

  this.setNewUser = async (data) => {
    if (!isUserAlreadyInList(data)) {
      this.setActiveUsers([...this._activeUsers, data]);
    }

    if (!isReadyToStart()) return;

    this.setGameData({
      status: 'in game',
      roomKing: this._activeUsers[0].id,
    });

    await this.getWord();
  }

  this.userDisconnected = (disconnectedId) => {
    this.setActiveUsers(this._activeUsers.filter(({ id }) => id !== disconnectedId));

    if (isGameCantExist()) {
      this.setGameData({
        status: 'looking for players',
        roomKing: '',
      });

      this.clearCanvas();
      this.setMessagesList([]);
    }
  }

  this.getMessagesList = () => {
    io.emit("messages list update", this._messagesList);
  }

  this.setMessagesList = (messages) => {
    this._messagesList = messages;

    io.emit("messages list update", messages);
  }

  this.setNewMessage = (data) => {
    if (isWordMatched(data)) {
      this.setGameData({
        status: 'game finished',
        roomKing: pickNewRoomKing(data),
      });

      this.clearCanvas();
      this.setMessagesList([]);

      // refresh game
      setTimeout(async () => {
        if (isEnoughUsersToStart()) {
          await this.getWord();

          this.setGameData({
            status: 'in game',
            roomKing: pickNewRoomKing(data),
          });
        } else {
          this.setGameData({
            status: 'looking for players',
            roomKing: '',
          })
        }
      }, 2000);
    } else {
      if (!isMessagesListFull()) {
        this.setMessagesList([...this._messagesList, data]);
      } else {
        let newList = this._messagesList;
        newList.shift();

        this.setMessagesList([...newList, data]);
      }
    }
  }

  this.updateMessage = (updatedMessage) => {
    this.setMessagesList(this._messagesList.map(message => message.id === updatedMessage.id
      ? {
        ...message,
        ...updatedMessage,
      }
      : message)
    );
  }

  this.getGameData = () => {
    io.emit('game status update', this._gameData);
  }

  this.setGameData = (data) => {
    this._gameData = data;

    io.emit("game status update", data);
  }

  // Word handlers

  const setNewWord = async () => {
    this.setWord(await getWord());
  }

  this.getWord = async () => {
    await setNewWord();

    io.emit('update word', this._word);
  }

  this.setWord = (word) => {
    this._word = word;
  }

  // Clear canvas
  this.clearCanvas = () => {
    io.emit('drawing update', {
      type: 'clear',
    })
  }
}

module.exports = State;
