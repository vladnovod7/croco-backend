const axios = require('axios');

const getWord = async () => {
  const { data } = await axios.get('http://free-generator.ru/generator.php?action=word&type=1');

  return data.word.word;
}

getWord();

module.exports.getWord = getWord;
