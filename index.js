require('dotenv').config();
const express = require("express");
const socket = require("socket.io");
const State = require("./State");
const { getWord } = require('./randomWordAPI');

// App setup
const app = express();
const server = app.listen(process.env.PORT, function () {
  console.log(`Listening on port ${process.env.PORT}`);
  console.log(`http://localhost:${process.env.PORT}`);
});

// Socket setup
const io = socket(server);

const state = new State({}, io);

messagesList = [];
gameData = {
  status: 'looking for players',
  roomKing: '',
}
word = '',

io.on("connection", function (socket) {
  console.log("Made socket connection");

  state.getActiveUsers();
  state.getMessagesList();

  socket.on('get word', state.getWord)

  socket.on("new user", async function (data) {
    socket.userId = data.id;
    state.setNewUser(data)
  });

  socket.on("update message", state.updateMessage);

  socket.on("new message", state.setNewMessage);

  socket.on('get messages list', state.getMessagesList);

  socket.on('draw start', function (data) {
    io.emit('drawing update', {
      type: 'drawStart',
      point: data,
    })
  });

  socket.on('new point', function (data) {
    io.emit('drawing update', {
      type: 'newPoint',
      point: data,
    })
  });

  socket.on('draw end', function () {
    io.emit('drawing update', {
      type: 'drawEnd',
    })
  });

  socket.on('clear', function () {
    io.emit('drawing update', {
      type: 'clear',
    })
  });

  socket.on('get game status', state.getGameData)

  socket.on("disconnect", () => {
    state.userDisconnected(socket.userId);
  });
});
